<?php

class Client extends Personne{
    private $adresse;

    public function __construct($nom,$prenom,$adresse)
    {
        parent::__construct($nom,$prenom);
        
        $this->adresse = $adresse;
    }

    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    public function getAdresse()
    {
        return $this->adresse;
    }

    public function getCoordonnees()
    {
        echo "je suis ".$this->getPrenom() ." ". $this->getNom() . " j'habite à " .$this->getAdresse();
    }
}
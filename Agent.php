<?php

class Agent extends Employe{
    public function __construct($nom,$prenom,$salaire)
    {
        $this->setNom($nom);
        $this->setPrenom($prenom);
        $this->setSalaire($salaire);
    }

    public function calculSalaire()
    {
        echo 'je suis '.$this->getNom(). " " . " et mon salaire est: ".$this->getSalaire();
    }


    public function prendreConge()
    {
        echo "je suis ".$this->getPrenom(). " ". " et mon congé est de 30 jours";
    }
}
<?php


class Produit{
    private $reference;
    private $name;
    private $price;

    private static $remise = 20;



    public function __construct($reference,$name,$price)
    {
        $this->setReference($reference);
        $this->setName($name);
        $this->setPrice($price);
    }

    public function setReference($reference)
    {
        $this->reference = $reference;
    }


    public function setName($name)
    {
        $this->name = $name;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }


    public function getReference()
    {
        return $this->reference;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }


    public function setRemise($remise)
    {
        self::$remise = $remise;
    }

    public static function getRemise()
    {
        return self::$remise;
    }

    public function getNewPrice()
    {
        return $this->price *(1-self::$remise/100);
    }
}


<?php


class Professeur implements Personnels,Fonctionnaire
{
    private $nom;
    private $anciennete;
    private $indice;

    public function __construct($nom,$anciennete,$indice)
    {
        $this->nom = $nom;
        $this->anciennete = $anciennete;
        $this->indice = $indice;
    }

    public function getSalaire()
    {
        return 1000*(1+$this->getIndice()*$this->getIndice()/100);
    }

    public function passerExamen()
    {
        echo 'je suis '.$this->getNom(). " ". " j'ai passé l'examen";
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function setAnciennete($anciennete)
    {
        $this->anciennete = $anciennete;
    }

    public function setIndice($indice)
    {
        $this->indice = $indice;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getAnciennete()
    {
        return $this->anciennete;
    }

    public function getIndice()
    {
        return $this->indice;
    }
}
<?php


class Voiture extends Vehicule{
    private $marque;
    private $volume;


    public function __construct($name,$moteur,$roue,$phare,$marque,$volume)
    {
        parent::__construct($name,$moteur,$roue,$phare);
        $this->marque = $marque;
        $this->volume = $volume;
    }

    public function setMarque($marque){
        $this->marque = $marque;
    }

    public function setVolume($volume){
        $this->volume = $volume;
    }

    public function getMarque(){
        return $this->marque;
    }

    public function getVolume(){
        return $this->volume;
    }
}
<?php

class Utilisateur{
    protected $nom;
    protected $prenom;
    protected $age = 23;
    protected $email;

    const NREF_BASSE = 10;
    const NREF_HAUTE = 15;

    public function __construct($nom,$prenom,$email)
    {
        $this->setNom($nom);
        $this->setPrenom($prenom);
        $this->setEmail($email);
    }

    public function SeConnecter()
    {
        echo 'je suis un utilisateur de votre application , je peux donc me connecter';
    }

    function SeDeconnecter()
    {
        echo 'je me deconnecte';
    }
    

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    public function setAge($age)
    {
        if ($age>120 || $age<0) {
            throw new Exception("l'age doit être entre 1 et 120");
        }else {
            
            $this->age = $age;
        }
    }

    public function setEmail($email)
    {
        if (filter_var($email,FILTER_VALIDATE_EMAIL)) {
            
            $this->email = $email;
        }
        else {
            throw new Exception("Attention, vous devez saisir une adresse mail valide");
            
        }
    }
    

    public function getNom()
    {
        return $this->nom;
    }


    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getAge()
    {
        return $this->age;
    }


    public function getEmail()
    {
        return $this->email;
    }

    

    /*public function degreImplication()
    {
        if ($this->getNote()>=self::NREF_HAUTE) {
            echo 'Votre contribution est excellente';
        }

        elseif($this->getNote()>=self::NREF_BASSE){
            echo 'Votre contribution est moyenne';
        }

        else {
            echo 'Votre contribution est mauvaise';
        }
    }*/
}
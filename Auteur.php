<?php

class Auteur extends Utilisateur{
    private $note;
    private $avis;

    public function __construct($nom,$prenom,$email,$note,$avis)
    {
        parent::__construct($nom,$prenom,$email);
        $this->note = $note;
        $this->avis = $avis;   
    }
    
    public function SeConnecter()
    {
        echo 'je suis '.$this->nom. " ". 'auteur de cette appli peux donc me connecter';
    }


    public function setNote($note)
    {
        $this->note = $note;
    }

    public function setAvis($avis)
    {
        $this->avis = $avis;
    }


    public function getNote()
    {
        return $this->note;
    }

    public function getAvis()
    {
        return $this->avis;
    }
}
<?php


class Vehicule {
    protected $name;
    protected $moteur;
    protected $roue;
    protected $phare;


    public function __construct($name,$moteur,$roue,$phare)
    {
        $this->name = $name;
        $this->moteur = $moteur;
        $this->roue = $roue;
        $this->phare = $phare;
    }


    public function setName($name){
        $this->name = $name;
    }

    public function setMoteur($moteur){
        $this->moteur = $moteur;
    }

    public function setRoue($roue){
        $this->roue = $roue;
    }

    public function setPhare($phare){
        $this->phare = $phare;
    }

    public function getName(){
        return $this->name;
    }

    public function getMoteur(){
        return $this->moteur;
    }

    public function getRoue(){
        return $this->roue;
    }

    public function getPhare(){
        return $this->phare;
    }
}
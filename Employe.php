<?php


abstract class Employe{
    protected $nom;
    protected $prenom;
    protected $salaire;

    abstract function calculSalaire();

    abstract function prendreConge();


    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }


    public function setSalaire($salaire)
    {
        $this->salaire = $salaire;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getSalaire()
    {
        return $this->salaire;
    }
}